package task2;

public class LargeNumberAdder {

    public static String addLargeNumbers(String num1, String num2) {
        StringBuilder result = new StringBuilder();

        int carry = 0;
        int p1 = num1.length() - 1;
        int p2 = num2.length() - 1;

        while (p1 >= 0 || p2 >= 0 || carry != 0) {
            int digit1, digit2;

            if (p1 >= 0) {
                digit1 = num1.charAt(p1) - '0';
            } else {
                digit1 = 0;
            }

            if (p2 >= 0) {
                digit2 = num2.charAt(p2) - '0';
            } else {
                digit2 = 0;
            }

            int sum = digit1 + digit2 + carry;
            result.append(sum % 10);
            carry = sum / 10;

            p1--;
            p2--;
        }

        return result.reverse().toString();
    }
}
