package task2;

public class Main {
    public static void main(String[] args) {
        String result = LargeNumberAdder.addLargeNumbers("123456789123456789", "987654321987654321");
        System.out.println("Sum: " + result);

        ArmstrongNumberFinder.findArmstrongNumbers(1000);

        int[] D = {1, 2, 3, 4, 5, 6, 7};
        ConsecutiveSum.calculateSums(D);

        int[] dice = {1, 2, 3};
        int[] cells = {-1, -1, 3, 4, 5, -1, -1};
        SuperLockSolver.findCombinations(dice, cells);

        boolean areCoprime = CoprimeChecker.areCoprime(35, 64, 27);
        System.out.println("Numbers 35, 64, and 27 are coprime: " + areCoprime);
    }
}


