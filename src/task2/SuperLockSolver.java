package task2;

public class SuperLockSolver {

    public static void findCombinations(int[] dice, int[] cells) {
        for (int i = 0; i < cells.length - 2; i++) {
            if (isCombinationSumTen(dice, cells, i)) {
                System.out.println("Combination found at positions: " + i + ", " + (i + 1) + ", " + (i + 2));
            }
        }
    }

    private static boolean isCombinationSumTen(int[] dice, int[] cells, int start) {
        int sum = 0;
        for (int i = 0; i < 3; i++) {
            if (cells[start + i] != -1) {
                sum += cells[start + i];
            } else if (dice.length > i) {
                sum += dice[i];
            } else {
                return false;
            }
        }
        return sum == 10;
    }
}

