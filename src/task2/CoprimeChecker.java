package task2;

public class CoprimeChecker {
    public static boolean areCoprime(int a, int b, int c) {
        return gcd(a, gcd(b, c)) == 1;
    }

    private static int gcd(int a, int b) {
        while (b != 0) {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }
}
