package task2;

public class ArmstrongNumberFinder {

    public static void findArmstrongNumbers(int k) {
        for (int i = 1; i <= k; i++) {
            if (isArmstrong(i)) {
                System.out.println(i + " is an Armstrong number.");
            }
        }
    }

    private static boolean isArmstrong(int number) {
        int original = number, sum = 0;
        int length = String.valueOf(number).length();

        while (number > 0) {
            int digit = number % 10;
            sum += (int) Math.pow(digit, length);
            number = number / 10;
        }

        return sum == original;
    }
}