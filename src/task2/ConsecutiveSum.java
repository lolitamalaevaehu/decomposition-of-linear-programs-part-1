package task2;

public class ConsecutiveSum {

    public static void calculateSums(int[] D) {
        for (int i = 0; i < D.length - 2; i++) {
            int sum = D[i] + D[i + 1] + D[i + 2];
            System.out.println("Sum of elements at indices " + i + ", " + (i + 1) + ", and " + (i + 2) + " is: " + sum);
        }
    }
}
