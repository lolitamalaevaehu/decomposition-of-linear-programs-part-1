package task1;

public class Main {
    public static int findGreatestCommonDivisor(int firstNumber, int secondNumber) {
        while (secondNumber != 0) {
            int temp = secondNumber;
            secondNumber = firstNumber % secondNumber;
            firstNumber = temp;
        }
        return firstNumber;
    }

    public static int findSmallestCommonMultiple(int firstNumber, int secondNumber) {
        int temp = firstNumber * secondNumber;
        return Math.abs(temp) / findGreatestCommonDivisor(firstNumber, secondNumber);
    }

    public static int findGreatestCommonDivisorForThree(int firstNumber, int secondNumber, int thirdNumber) {
        //Used pairDivisor for finding GCD for two numbers
        int pairDivisor = findGreatestCommonDivisor(firstNumber, secondNumber);
        while (thirdNumber != 0) {
            int temp = thirdNumber;
            thirdNumber = pairDivisor % thirdNumber;
            pairDivisor = temp;
        }
        return pairDivisor;
    }

    public static int calculateSumOfOddFactorials() {
        int sum = 0;
        for (int i = 1; i <= 9; i += 2) { // Loop through only odd numbers
            sum += factorial(i);
        }
        return sum;
    }

    // Helper method to calculate factorial of a number
    private static int factorial(int number) {
        int result = 1;
        for (int factor = 2; factor <= number; factor++) {
            result *= factor;
        }
        return result;
    }

    public static int secondMaximum(int[] digits) {
        // if array contain less than 2 elements
        if (digits.length < 2) {
            throw new IllegalArgumentException("Array must contain at least two elements.");
        }

        int max = Math.max(digits[0], digits[1]);
        int secondMax = Math.min(digits[0], digits[1]);

        for (int i = 2; i < digits.length; i++) {
            if (digits[i] > max) {
                secondMax = max;
                max = digits[i];
            } else if (digits[i] > secondMax && digits[i] != max) {
                secondMax = digits[i];
            }
        }

        return secondMax;
    }

    public static void main(String[] args) {
        int[] array = new int[]{21, 20, 4, 3, 9, 65, 34};
        System.out.println("The second max element is: " + secondMaximum(array));
        System.out.println("The Greatest Common Divisor is: " + findGreatestCommonDivisor(15, 4));
        System.out.println("The Smallest Common Multiple is: " + findSmallestCommonMultiple(15, 4));
        System.out.println("The Greatest Common Divisor for three numbers is: " + findGreatestCommonDivisorForThree(15, 155, 344));
        System.out.println("The sum of factorials of all odd numbers from 1 to 9 is: " + calculateSumOfOddFactorials());

    }


}
