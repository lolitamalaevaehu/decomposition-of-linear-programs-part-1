## Overview

This project consists of a series of methods written in Java, designed to perform various mathematical computations. These include finding the greatest common divisor (GCD) and the smallest common multiple (SCM) of natural numbers, summing factorials of odd numbers, and identifying the second-largest number in an array.

## Task 1

### 1. Greatest Common Divisor and Smallest Common Multiple

- **GCD**: The method finds the greatest common divisor of two natural numbers.
- **SCM**: The method calculates the smallest common multiple of two natural numbers.

### 2. Greatest Common Divisor of Four Numbers

- This method extends the GCD computation to four natural numbers.

### 3. Smallest Common Multiple of Three Numbers

- This method calculates the smallest common multiple for three natural numbers.

### 4. Sum of Factorials

- The method calculates the sum of factorials of all odd numbers from 1 to 9.

### 5. Second-Largest Number in an Array

- This method finds the second-largest number in a given array `A[N]`.


## Task 2

### Solve the tasks:

1. Write a  method(s) that checks whether the given three numbers are coprime.

2. Implement the addition of two numbers of unlimited length. Use the String type to store numbers. To solve the problem, use the division of code into methods.

3. Armstrong number is a number that is the sum of its own digits each raised to the power of the number of digits. Find all Armstrong numbers from 1 to k. To solve the problem, use the division of code into methods. To solve the problem, use the division of code into methods.

4. An array D is given. Determine the following sums: D[l] + D[2] + D[3]; D[3] + D[4] + D[5]; D[4] +D[5] +D[6].

Explanation. Compose a method(s) for calculating the sum of three consecutive array elements with numbers from k to m.

5. «Super Lock». The secret lock for the safe consists of 10 cells arranged in a row, into which you need to insert dice. But the door opens only if in any three neighboring cells the sum of points on the front faces of the dice is 10. (A dice has from 1 to 6 points on each face). Write a program that solves the code of the lock, provided that two dice are already inserted into the cells. To solve the problem, use the division of code into methods.

### Task Solving Memo:

Read the task condition several times.
Create an algorithm for solving a problem with several data sets (for example, integers and real numbers, sets with negative numbers, etc.).
Pay attention to the maximum allowable values.
Try to simplify the algorithm (if possible and understand how to do it)Simplify the algorithm (you can use pseudocode).
Write code in Java. Use comments.
Test the code using several input data sets in accordance with paragraph 2 and make sure that the written code is working properly.
The task is considered solved if the year was completed successfully in accordance with the condition and the output parameters correspond to the input parameters.
Place the solved task in an open Git repository.